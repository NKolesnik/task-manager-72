package ru.t1consulting.nkolesnik.tm.exception.task;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class TaskEmptyIdException extends AbstractException {

    public TaskEmptyIdException() {
        super("Error! Task id is empty...");
    }

}
