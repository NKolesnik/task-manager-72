package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.List;

public interface IProjectDtoService {

    ProjectDto create(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable String id);

    @Nullable ProjectDto findById(@Nullable final String userId, @Nullable String id);

    @Nullable List<ProjectDto> findAll(@Nullable final String userId);

    long count(@Nullable final String userId);

    void save(@Nullable final String userId, @Nullable ProjectDto project);

    void deleteById(@Nullable final String userId, @Nullable String id);

    void delete(@Nullable final String userId, @Nullable ProjectDto project);

    void deleteAll(@Nullable final String userId, @Nullable List<ProjectDto> projectList);

    void clear(@Nullable final String userId);

}
