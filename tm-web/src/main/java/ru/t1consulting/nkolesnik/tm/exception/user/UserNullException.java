package ru.t1consulting.nkolesnik.tm.exception.user;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class UserNullException extends AbstractException {

    public UserNullException() {
        super("Error! User is null...");
    }

}
