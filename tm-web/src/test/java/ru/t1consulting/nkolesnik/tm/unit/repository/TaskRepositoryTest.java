package ru.t1consulting.nkolesnik.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.config.ApplicationConfiguration;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.Collections;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();
    @NotNull
    @Autowired
    private ITaskDtoRepository repository;
    @NotNull
    private TaskDto task;

    @Before
    public void setUp() {
        task = new TaskDto();
        task.setName("Task");
        task.setUserId(USER_ID);
        repository.save(task);
    }

    @After
    public void tearDown() {
        repository.delete(task);
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertTrue(repository.existsByUserIdAndId(USER_ID, task.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(null, task.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER_ID, null));
        Assert.assertFalse(repository.existsByUserIdAndId(null, null));
    }

    @Test
    public void findByUserIdAndId() {
        Assert.assertNotNull(repository.findByUserIdAndId(USER_ID, task.getId()));
        Assert.assertNull(repository.findByUserIdAndId(null, task.getId()));
        Assert.assertNull(repository.findByUserIdAndId(USER_ID, null));
        Assert.assertNull(repository.findByUserIdAndId(null, null));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(1L, repository.countByUserId(USER_ID));
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
        repository.deleteByUserIdAndId(USER_ID, task.getId());
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
        repository.deleteByUserIdAndId(USER_ID, task.getId());
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
    }

}
