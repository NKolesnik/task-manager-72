package ru.t1consulting.nkolesnik.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        @NotNull final TaskUnbindFromProjectResponse response = getProjectTaskEndpoint().unbindTaskFromProject(request);
    }

}
