package ru.t1consulting.nkolesnik.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(@Nullable String token) {
        super(token);
    }

    public UserRegistryRequest(
            @Nullable String token,
            @Nullable String login,
            @Nullable String password,
            @Nullable String email) {
        super(token);
        this.login = login;
        this.password = password;
        this.email = email;
    }

}
