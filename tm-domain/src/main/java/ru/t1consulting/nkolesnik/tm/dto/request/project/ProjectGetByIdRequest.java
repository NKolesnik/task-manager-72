package ru.t1consulting.nkolesnik.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectGetByIdRequest(@Nullable String token) {
        super(token);
    }

    public ProjectGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
