package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDto;


@Repository
public interface AbstractDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

}
