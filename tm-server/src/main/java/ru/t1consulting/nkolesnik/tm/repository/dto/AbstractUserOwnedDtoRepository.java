package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDto;

@Repository
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends
        AbstractDtoRepository<M> {

}
